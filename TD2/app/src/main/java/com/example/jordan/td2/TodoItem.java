package com.example.jordan.td2;

/**
 * Created by jordan on 16/02/2018.
 */

public class TodoItem {
    String label;
    public enum Tags {
        Faible,
        Normal,
        Important;
    }
    private Tags tag;
    boolean done = false;

    public TodoItem(Tags tag, String label) {
        this.tag = tag;
        this.label = label;
        this.done = false;
    }

    public String getLabel() {
        return label;
    }

    public Tags getTag() {
        return tag;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public void setTag(Tags tag) {
        this.tag = tag;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
